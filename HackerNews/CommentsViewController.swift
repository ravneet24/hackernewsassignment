//
//  commentsViewController.swift
//  HackerNews
//
//  Created by Ravneet Arora on 01/02/17.
//  Copyright © 2017 Ravneet Arora. All rights reserved.
//

import UIKit
import MBProgressHUD

class commentsViewController: UIViewController {
    @IBOutlet weak var tblViewComments: UITableView!
    var commentsIdsArray:Array = [Any]()
    var chunksCommentIdsArray:Array = [Any]()
    var commentsInformationArray:Array = [CommentsModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Take First Six ids if id count is more than or equal to  6
        if commentsIdsArray.count > 5 {
            chunksCommentIdsArray.append(contentsOf:commentsIdsArray[0..<6])
        }else {
           
            //Take ids count if it is less than  6
            chunksCommentIdsArray.append(contentsOf:commentsIdsArray[0..<commentsIdsArray.count])
        }
        
        if chunksCommentIdsArray.count > 0 {
            self.showIndicator()
            self.fetchCommentsOfStories()
        }
    }
    //Mark: Private Methods
    // Call this to fetch comments of stories
    func fetchCommentsOfStories() -> Void {
        
        TopStoriesManager.fetchStoryInformationOfItem(itemId: "\(chunksCommentIdsArray[0] as! NSNumber)" ,completion:{ (isSucceeded,responseObject) -> Void in
            
            if isSucceeded == true {
                print(isSucceeded!)
                
                if let responseDictionary = responseObject as? Dictionary<String, AnyObject> {
                    let CommentsModelObj = CommentsModel.init(responseDictionary)
                    //For deleted comments text is not coming
                    if CommentsModelObj.text != nil {
                        self.commentsInformationArray.append(CommentsModelObj)
                    }
                    
                    self.chunksCommentIdsArray.remove(at: 0)
                    
                    if self.chunksCommentIdsArray.count > 0 {
                        self.fetchCommentsOfStories()
                    }else {
                        DispatchQueue.main.async {
                            self.hideActivityIndicator()
                            self.tblViewComments.reloadData()
                        }
                    }
                }
            }else {
                self.hideActivityIndicator()
            }
        })
    }
    
    func showIndicator() {
        MBProgressHUD.showAdded(to: self.view, animated: true)
    }
    
    func hideActivityIndicator() {
        DispatchQueue.main.async {
            MBProgressHUD.hide(for: self.view, animated: true)
        }
    }
}

extension commentsViewController:UITableViewDelegate,UITableViewDataSource {
    //MARK: Tableview Datasources
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if commentsInformationArray.count > 0 {
            return commentsInformationArray.count
        }else {
            return 6
        }
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier:String = "CellStories"
        let cell:customStoriesTableViewCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! customStoriesTableViewCell
        
        if commentsInformationArray.count > 0 {
            
            cell.lbl_Title.text = "\(commentsInformationArray[indexPath.row].text!)"
            
            if commentsInformationArray[indexPath.row].kids != nil  {
                cell.btn_comments.isEnabled = true
                cell.btn_comments.setTitle("\((commentsInformationArray[indexPath.row]).kids!.count)" + " comments",for: UIControlState.normal)
            }else {
                cell.btn_comments.isEnabled = false
                cell.btn_comments.setTitle(" No comments", for: UIControlState.normal)
            }
            cell.btn_comments.tag = indexPath.row
        }
        return cell;
        
    }
    
    //MARK: Scroll View Delegate
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        if scrollView == tblViewComments {
            
            if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height) {
                
                if commentsIdsArray.count != commentsInformationArray.count  {
                    
                    if commentsIdsArray.count >= commentsInformationArray.count+6  {
                        chunksCommentIdsArray.append(contentsOf: commentsIdsArray[commentsInformationArray.count..<commentsInformationArray.count+6])
                    }else {
                        chunksCommentIdsArray.append(contentsOf: commentsIdsArray[commentsInformationArray.count..<commentsIdsArray.count])
                    }
                    self.showIndicator()
                    fetchCommentsOfStories()
                }
            }
        }
    }
}


