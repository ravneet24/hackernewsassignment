//
//  TopStoriesModel.swift
//  HackerNews
//
//  Created by ravneet.arora on 2/2/17.
//  Copyright © 2017 Ravneet Arora. All rights reserved.
//

import Foundation

class TopStoriesModel: NSObject {
    var id:String?
    var kids:Array<Any>?
    var title:String?
    var type:String?
  
      init(_ parameters: Dictionary<String, Any>) {
        id = parameters["id"] as? String
        kids = parameters["kids"] as? Array
        title = parameters["title"] as? String
        type = parameters["type"] as? String
        
    }
    
}
