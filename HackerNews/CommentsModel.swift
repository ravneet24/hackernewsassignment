//
//  CommentsModel.swift
//  HackerNews
//
//  Created by ravneet.arora on 2/3/17.
//  Copyright © 2017 Ravneet Arora. All rights reserved.
//

import Foundation

class CommentsModel: NSObject {
    
    var kids:Array<Any>?
    var text:String?
    var deleted:String?
    
    init(_ parameters: Dictionary<String, Any>) {
       
        kids = parameters["kids"] as? Array
        text = parameters["text"] as? String
        deleted = parameters["deleted"] as? String
    }
    
}
