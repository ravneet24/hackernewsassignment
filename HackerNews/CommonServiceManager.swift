//
//  commonServiceManager.swift
//  HackerNews
//
//  Created by Ravneet Arora on 01/02/17.
//  Copyright © 2017 Ravneet Arora. All rights reserved.
//

import Foundation

enum WebserviceType:String
{
    case GET  = "GET"
    case POST = "POST"
}

class Parameter
{
    var methodName: String!
    var requestType: WebserviceType
    var postDict: NSDictionary?
    init(serviceType:WebserviceType) {
        self.requestType = serviceType
    }
}

class CommonServiceManager: NSObject
{
    static func getBasePath() -> String {
        return "https://hacker-news.firebaseio.com/v0/"
    }
    
    class func requestUrl (parameter: Parameter, completion: @escaping (_ data: AnyObject?, _ error: NSError?) -> (Void)) throws -> URLSessionTask?{
        
        let targetURLString = String(format: "%@%@", getBasePath(),parameter.methodName)
        let request = NSMutableURLRequest(url: NSURL(string: targetURLString as String)! as URL)
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        
        switch parameter.requestType
        {
        case WebserviceType.GET:
            request.httpMethod = parameter.requestType.rawValue
        case WebserviceType.POST:
            let jsonData = try JSONSerialization.data(withJSONObject: parameter.postDict!, options: .prettyPrinted)
            request.httpMethod = parameter.requestType.rawValue
            request.httpBody = jsonData
            /*default:
             print(targetURLString)
             */
        }
        
        let session = URLSession.shared
        let task: URLSessionDataTask = session.dataTask(with: request as URLRequest) { (data, response, error) -> Void in
            let statusCode = (response as! HTTPURLResponse).statusCode
            
            if statusCode == 200 {
                do{
                    let responseObj = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers)
                    completion(responseObj as AnyObject?, error as NSError?)
                    
                }catch{
                    print("Could not convert JSON data")
                }
            }else {
                completion(nil, error as NSError?)
            }
        }
        task .resume()
        return task
    }
}
