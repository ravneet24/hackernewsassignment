//
//  ViewController.swift
//  HackerNews
//
//  Created by Ravneet Arora on 01/02/17.
//  Copyright © 2017 Ravneet Arora. All rights reserved.
//

import UIKit
import MBProgressHUD
class TopStoriesViewController: UIViewController {
    
    @IBOutlet weak var tblView_topStories: UITableView!
    
    var totalTopStoriesIdArray:Array = [Any]()
    var chunksTopStoriesIdArray:Array = [Any]()
    var TopStoriesInformationArray:Array = [TopStoriesModel]()
    var isThroughPullToRefresh:Bool = false
    var commentsArrayIds:Array = [Any]()
    let refreshControl = UIRefreshControl()
    var newTopStoriesIds:Array = [Any]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Refresh Control
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(refresh(sender:)), for: UIControlEvents.valueChanged)
        tblView_topStories.addSubview(refreshControl)
        fetchTopStoriesIds()
    }
    
    //MARK: Private Methods
    func fetchTopStoriesIds() -> Void {
        //Call top stories id Service
        showIndicator()
        TopStoriesManager.fetchTopStoriesId(completion:{
            (isSuceeded,data) -> Void in
            
            if isSuceeded == true {
                if let responseArray = data as? Array<Any> {
                    
                    if self.isThroughPullToRefresh {
                       self.isThroughPullToRefresh = false
                        
                        for newIds in responseArray {
                            
                            if !((self.totalTopStoriesIdArray as NSArray).contains(newIds)) {
                               self.newTopStoriesIds.append(newIds)
                            }
                        }
                        
                        if self.newTopStoriesIds.count > 0 {
                            self.totalTopStoriesIdArray.insert(self.newTopStoriesIds, at: 0)
                            self.fetchNewStories()
                        }else {
                            self.hideActivityIndicator()
                        }
                        
                    }else {
                         self.totalTopStoriesIdArray.append(contentsOf: responseArray)
                         self.fetchNewStories()
                    }
                }else {
                    self.hideActivityIndicator()
                }
                }
                
        })
    }
    
    func fetchNewStories() {
        
        //Take First Six ids if id count is more than or equal to  6
        if self.totalTopStoriesIdArray.count > 5 {
            self.chunksTopStoriesIdArray.append(contentsOf:self.totalTopStoriesIdArray[0..<6])
        }else {
            //Take ids count if it is less than  6
            self.chunksTopStoriesIdArray.append(contentsOf:self.totalTopStoriesIdArray[0..<self.totalTopStoriesIdArray.count])
        }
        
        if self.chunksTopStoriesIdArray.count > 0 {
            
            self.fetchInformationOfStories()
        }else {
            self.hideActivityIndicator()
        }
    }
    ///call to get information of any story id
    func fetchInformationOfStories() -> Void {
        
        TopStoriesManager.fetchStoryInformationOfItem(itemId: "\(self.chunksTopStoriesIdArray[0] as! NSNumber)" ,completion:{ (isSucceeded,responseObject) -> Void in
            
            if isSucceeded == true {
                print(isSucceeded!)
                
                if let responseDictionary = responseObject as? Dictionary<String, AnyObject> {
                    let topStoriesModelObj = TopStoriesModel.init(responseDictionary)
                    self.TopStoriesInformationArray.append(topStoriesModelObj)
                    self.chunksTopStoriesIdArray.remove(at: 0)
                    
                    if self.chunksTopStoriesIdArray.count > 0 {
                        self.fetchInformationOfStories()
                    }else {
                        DispatchQueue.main.async {
                            self.hideActivityIndicator()
                            self.tblView_topStories.reloadData()
                        }
                    }
                }
            }else {
                self.hideActivityIndicator()
            }
        })
    }
    
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "segue_comments") {
            
            // segues attached to same view
            let detailVC = segue.destination as! commentsViewController;
           
            if commentsArrayIds.count > 0 {
                detailVC.commentsIdsArray = commentsArrayIds
            }
            
        }
    }
    
    func showIndicator() {
        MBProgressHUD.showAdded(to: self.view, animated: true)
    }
    
    func hideActivityIndicator() {
        DispatchQueue.main.async {
            MBProgressHUD.hide(for: self.view, animated: true)
        }
    }
    //MARK: Action Methods
    func refresh(sender:AnyObject) {
        // refresh table view
        isThroughPullToRefresh = true
        fetchTopStoriesIds()
        refreshControl.endRefreshing()
        
    }
    
}

extension TopStoriesViewController:UITableViewDelegate,UITableViewDataSource {
    //MARK: Tableview Datasources
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if TopStoriesInformationArray.count > 0 {
            return TopStoriesInformationArray.count
        }else {
            return 6
        }
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier:String = "CellStories"
        let cell:customStoriesTableViewCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! customStoriesTableViewCell
        cell.delegate = self
        
        if TopStoriesInformationArray.count > 0 {
            cell.lbl_Title.text = "\(TopStoriesInformationArray[indexPath.row].title!)"
            
            if TopStoriesInformationArray[indexPath.row].kids != nil  {
                cell.btn_comments.isEnabled = true
                cell.btn_comments.setTitle("\(TopStoriesInformationArray[indexPath.row].kids!.count)" + " comments",for: UIControlState.normal)
            }else {
                cell.btn_comments.isEnabled = false
                cell.btn_comments.setTitle(" No comments", for: UIControlState.normal)
            }
            cell.btn_comments.tag = indexPath.row
        }
        return cell;
        
    }
    
    //MARK: Scroll View Delegate
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        if scrollView == tblView_topStories{
            
            if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height) {
               
                if totalTopStoriesIdArray.count != TopStoriesInformationArray.count {
                   
                    if totalTopStoriesIdArray.count >= TopStoriesInformationArray.count+6  {
                        chunksTopStoriesIdArray.append(contentsOf: totalTopStoriesIdArray[TopStoriesInformationArray.count..<TopStoriesInformationArray.count+6])
                    }else {
                        chunksTopStoriesIdArray.append(contentsOf: totalTopStoriesIdArray[TopStoriesInformationArray.count..<totalTopStoriesIdArray.count])
                    }
                    showIndicator()
                    fetchInformationOfStories()
                }
               
            }
        }
    }
}

extension TopStoriesViewController:customStoriesTableViewCellDelegate {
    
    func btn_showComments(_ sender: Any) {
        let btn_Comment = sender as! UIButton
        commentsArrayIds = TopStoriesInformationArray[btn_Comment.tag].kids!
        performSegue(withIdentifier: "segue_comments", sender: nil)
        print("comment button pressed")
    }
    
}
