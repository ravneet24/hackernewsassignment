//
//  customStoriesTableViewCell.swift
//  HackerNews
//
//  Created by Ravneet Arora on 01/02/17.
//  Copyright © 2017 Ravneet Arora. All rights reserved.
//

import UIKit
protocol customStoriesTableViewCellDelegate {
      func btn_showComments(_ sender: Any)
}

class customStoriesTableViewCell: UITableViewCell {
    @IBOutlet weak var btn_comments: UIButton!
    @IBOutlet weak var lbl_Title: UILabel!
    
    var delegate:customStoriesTableViewCellDelegate?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    //Mark:Action Methods
    @IBAction func btn_showComments(_ sender: Any) {
        delegate?.btn_showComments(sender) 
    }
}
