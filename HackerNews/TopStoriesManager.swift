//
//  TopStoriesManager.swift
//  HackerNews
//
//  Created by Ravneet Arora on 01/02/17.
//  Copyright © 2017 Ravneet Arora. All rights reserved.
//

import Foundation

class TopStoriesManager:NSObject {
  
    //call this to get Top stories
    class func fetchTopStoriesId(completion:@escaping (Bool?,AnyObject) -> (Void)!) {
        
        let parameterObj = Parameter(serviceType: WebserviceType.GET)
        parameterObj.methodName = "topstories.json"
        
        do
        {
            _ =  try CommonServiceManager.requestUrl(parameter: parameterObj, completion: { (data, error) -> (Void) in
                print(data!)
                
                if data != nil && error == nil {
                    completion(true,data!)
                }
            })
        }
        catch
        {
            print("Top Stories Service Error")
        }
    }
    
    //call this to get full information of Particular Story.
    class func fetchStoryInformationOfItem(itemId:String!,completion:@escaping (Bool?,AnyObject) -> (Void)!) {
        let parameterObj = Parameter(serviceType: WebserviceType.GET)
        parameterObj.methodName = "item/" +  itemId + ".json"
        
        do
        {
            _ =  try CommonServiceManager.requestUrl(parameter: parameterObj, completion: { (data, error) -> (Void) in
                print(data!)
                if data != nil && error == nil {
                    completion(true,data!)
                }
                
            })
        }
        catch
        {
            print(" Fetch Story Information Service Error")
        }
    }
}
